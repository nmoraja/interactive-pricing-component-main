# Frontend Mentor - Interactive pricing component solution

This is a solution to the [Interactive pricing component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-pricing-component-t0m8PIyY8). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Use the slider and toggle to see prices for different page view numbers

### Screenshot

![](./screenshot.jpg)

### Links

- Solution URL: [https://bitbucket.org/nmoraja/interactive-pricing-component-main/src/master/](https://bitbucket.org/nmoraja/interactive-pricing-component-main/src/master/)
- Live Site URL: [https://nmorajda-interactive-pricing-component.netlify.app/](https://nmorajda-interactive-pricing-component.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- JavaScript ES6

## Author

- Website - [N. Morajda](https://abmstudio.pl)
- Frontend Mentor - [@nmorajda](https://www.frontendmentor.io/profile/nmorajda)
- Github - [nmorajda](https://github.com/nmorajda)
- Bitbucket - [nmorajda](https://bitbucket.org/nmoraja/)

## Acknowledgments

- Styling Cross-Browser Compatible Range Inputs with CSS: [https://css-tricks.com/styling-cross-browser-compatible-range-inputs-css/](https://css-tricks.com/styling-cross-browser-compatible-range-inputs-css/)
