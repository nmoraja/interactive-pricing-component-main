const offer = [
  {
    price: 8,
    pageViews: 25,
  },
  {
    price: 12,
    pageViews: 50,
  },
  {
    price: 16,
    pageViews: 100,
  },
  {
    price: 20,
    pageViews: 150,
  },
  {
    price: 24,
    pageViews: 200,
  },

  {
    price: 30,
    pageViews: 300,
  },
];

const elemPriceValue = document.getElementById('priceValue');
const elemPageViewValue = document.getElementById('pageViewValue');
const btnToggle = document.getElementById('btnToggle');
const inpRange = document.querySelector('input[type="range"]');
const progressBar = document.getElementById('progressBar');

const YearlyDiscount = 0.25;

let state = {
  offerIndex: 2,
  discount: false,
};

const updateState = newState => {
  state = { ...state, ...newState };
  updateUi();
};

// Create our number formatter.
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

const totalPrice = () => {
  const { offerIndex: index } = state;
  const { price } = offer[index];
  return state.discount ? price - price * YearlyDiscount : price;
};

const updateUi = () => {
  inpRange.setAttribute('min', 0);
  inpRange.setAttribute('max', offer.length - 1);
  inpRange.setAttribute('value', state.offerIndex);
  inpRange.setAttribute('aria-valuenow', state.offerIndex); // include for accessibility

  const progress = 100 / (offer.length - 1);
  progressBar.style.width = state.offerIndex * progress + '%';

  elemPageViewValue.textContent = offer[state.offerIndex].pageViews;
  elemPriceValue.textContent = formatter.format(totalPrice());
  // console.log(state);
};

btnToggle.addEventListener('click', () => {
  updateState({ discount: !state.discount });
  btnToggle.classList.toggle('is-active');
});

// https://stackoverflow.com/questions/18544890/onchange-event-on-input-type-range-is-not-triggering-in-firefox-while-dragging
['input', 'change'].forEach(event => {
  inpRange.addEventListener(event, e => {
    updateState({ offerIndex: e.target.value });
  });
});

updateUi();
